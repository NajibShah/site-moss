(function ($) {
  'use strict';
  var $window = $(window);
  $window.on('load', function () {
    $('#preloader').fadeOut('slow', function () {
      $(this).remove();
    });
  });
  $window.on('resizeEnd', function () {
    $('.full_height').height($window.height());
  });
  $window
    .on('resize', function () {
      if (this.resizeTO) clearTimeout(this.resizeTO);
      this.resizeTO = setTimeout(function () {
        $(this).trigger('resizeEnd');
      }, 300);
    })
    .trigger('resize');
  $window.on('scroll', function () {
    if ($window.scrollTop() > 0) {
      $('.header-area').addClass('sticky');
    } else {
      $('.header-area').removeClass('sticky');
    }
  });
  $('#menuIcon').on('click', function () {
    $('body').toggleClass('menu-open');
  });
  $('.closeIcon').on('click', function () {
    $('body').removeClass('menu-open');
  });
  $('[data-toggle="tooltip"]').tooltip();
  if ($.fn.niceScroll) {
    $('body, textarea').niceScroll({
      cursorcolor: '#151515',
      cursorwidth: '6px',
      background: '#f0f0f0',
    });
  }
  if ($.fn.owlCarousel) {
    var welcomeSlide = $('.hero-slides');
    $('.hero-slides').owlCarousel({
      items: 4,
      margin: 0,
      loop: true,
      dots: true,
      autoplay: true,
      autoplayTimeout: 5000,
      smartSpeed: 1000,
      responsive: {
        0: { items: 1 },
        576: { items: 2 },
        992: { items: 3 },
        1600: { items: 4 },
      },
    });
    welcomeSlide.on('translate.owl.carousel', function () {
      var slideLayer = $('[data-animation]');
      slideLayer.each(function () {
        var anim_name = $(this).data('animation');
        $(this)
          .removeClass('animated ' + anim_name)
          .css('opacity', '0');
      });
    });
    welcomeSlide.on('translated.owl.carousel', function () {
      var slideLayer = welcomeSlide
        .find('.owl-item.active')
        .find('[data-animation]');
      slideLayer.each(function () {
        var anim_name = $(this).data('animation');
        $(this)
          .addClass('animated ' + anim_name)
          .css('opacity', '1');
      });
    });
    $('[data-delay]').each(function () {
      var anim_del = $(this).data('delay');
      $(this).css('animation-delay', anim_del);
    });
    $('[data-duration]').each(function () {
      var anim_dur = $(this).data('duration');
      $(this).css('animation-duration', anim_dur);
    });
    $('.testimonial-slides').owlCarousel({
      items: 1,
      margin: 0,
      loop: true,
      dots: true,
      autoplay: true,
      autoplayTimeout: 5000,
      smartSpeed: 1000,
    });
  }
  if ($.fn.barfiller) {
    $('#bar1').barfiller({
      tooltip: true,
      duration: 1000,
      barColor: '#1d1d1d',
      animateOnResize: true,
    });
    $('#bar2').barfiller({
      tooltip: true,
      duration: 1000,
      barColor: '#1d1d1d',
      animateOnResize: true,
    });
    $('#bar3').barfiller({
      tooltip: true,
      duration: 1000,
      barColor: '#1d1d1d',
      animateOnResize: true,
    });
    $('#bar4').barfiller({
      tooltip: true,
      duration: 1000,
      barColor: '#1d1d1d',
      animateOnResize: true,
    });
  }
  $('.portfolio-menu button.btn').on('click', function () {
    $('.portfolio-menu button.btn').removeClass('active');
    $(this).addClass('active');
  });
  if ($.fn.imagesLoaded) {
    $('.sonar-portfolio').imagesLoaded(function () {
      $('.portfolio-menu').on('click', 'button', function () {
        var filterValue = $(this).attr('data-filter');
        $grid.isotope({ filter: filterValue });
      });
      var $grid = $('.sonar-portfolio').isotope({
        itemSelector: '.single_gallery_item',
        percentPosition: true,
        masonry: { columnWidth: '.single_gallery_item' },
      });
    });
  }
  if ($.fn.magnificPopup) {
    $('.gallery-img').magnificPopup({ type: 'image' });
  }
  if ($.fn.matchHeight) {
    $('.equalize').matchHeight({ byRow: true, property: 'height' });
  }
  if ($.fn.counterUp) {
    $('.counter').counterUp({ delay: 10, time: 2000 });
  }
  if ($.fn.scrollUp) {
    $.scrollUp({
      scrollSpeed: 1000,
      easingType: 'easeInOutQuart',
      scrollText: '<i class="fa fa-angle-up" aria-hidden="true"></i>',
    });
  }
  $("a[href='#']").on('click', function ($) {
    $.preventDefault();
  });
  if ($window.width() > 767) {
    new WOW().init();
  }
})(jQuery);
