import React, { Component } from 'react';
import Footer from '../layout/Footer';
import Loader from '../components/Loader';
import Navigation from '../layout/Navigation';
import HomeSlider from '../components/HomeSlider';

class Home extends Component {
  render() {
    return (
      <div>
        <div>
          <Loader />
          <Navigation />

          <HomeSlider className='hero-area ' />

          <div className='portfolio-area section-padding-100'>
            <div className='container'>
              <div className='row'>
                <div className='col-12'>
                  <div className='portfolio-title'>
                    <h2>
                      “Not so far away, behind the Margallah hills, far from
                      incompetence and boredom, live a group of people that
                      focus on both <span>Creativity & Delivery</span> ."
                    </h2>
                  </div>
                </div>
              </div>
              <div className='row justify-content-between'>
                <div className='col-12 col-md-5'>
                  <div className='single-portfolio-item mt-100 portfolio-item-1 wow fadeIn'>
                    <div className='backend-content'>
                      <img
                        className='dots'
                        src='img/core-img/dots.png'
                        alt=''
                      />
                      <h2>Reality</h2>
                    </div>
                    <div className='portfolio-thumb'>
                      <img src='img/bg-img/p1.png' alt='' />
                    </div>
                    <div className='portfolio-meta'>
                      {/* <p className='portfolio-date'>Feb 02, 2018</p> */}
                      <h2>Residential</h2>
                    </div>
                  </div>
                </div>
                <div className='col-12 col-md-6'>
                  <div className='single-portfolio-item mt-230 portfolio-item-2 wow fadeIn'>
                    <div className='backend-content'>
                      <img
                        className='dots'
                        src='img/core-img/dots.png'
                        alt=''
                      />
                    </div>
                    <div className='portfolio-thumb'>
                      <img src='img/bg-img/p2.png' alt='' />
                    </div>
                    <div className='portfolio-meta'>
                      {/* <p className='portfolio-date'>Feb 02, 2018</p> */}
                      <h2>Commercial</h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className='row'>
                <div className='col-12 col-md-10'>
                  <div className='single-portfolio-item mt-100 portfolio-item-3 wow fadeIn'>
                    <div className='backend-content'>
                      <img
                        className='dots'
                        src='img/core-img/dots.png'
                        alt=''
                      />
                      <h2>Architecture</h2>
                    </div>
                    <div className='portfolio-thumb'>
                      <img src='img/bg-img/p3.png' alt='' />
                    </div>
                    <div className='portfolio-meta'>
                      {/* <p className='portfolio-date'>Feb 02, 2018</p> */}
                      <h2>Interior</h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className='row justify-content-end'>
                <div className='col-12 col-md-6'>
                  <div className='single-portfolio-item portfolio-item-4 wow fadeIn'>
                    <div className='backend-content'>
                      <img
                        className='dots'
                        src='img/core-img/dots.png'
                        alt=''
                      />
                    </div>
                    <div className='portfolio-thumb'>
                      <img src='img/bg-img/p2.png' alt='' />
                    </div>
                    <div className='portfolio-meta'>
                      {/* <p className='portfolio-date'>Feb 02, 2018</p> */}
                      <h2>Hospitality</h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className='row'>
                <div className='col-12 col-md-5'>
                  <div className='single-portfolio-item portfolio-item-5 wow fadeIn'>
                    <div className='backend-content'>
                      <img
                        className='dots'
                        src='img/core-img/dots.png'
                        alt=''
                      />
                      <h2>Fantasy</h2>
                    </div>
                    <div className='portfolio-thumb'>
                      <img src='img/bg-img/p5.png' alt='' />
                    </div>
                    <div className='portfolio-meta'>
                      {/* <p className='portfolio-date'>Feb 02, 2018</p> */}
                      <h2>Concept</h2>
                    </div>
                  </div>
                </div>
              </div>
              <div className='row justify-content-center'>
                <div className='col-12 col-md-4'>
                  <div className='single-portfolio-item portfolio-item-6 wow fadeIn'>
                    <div className='portfolio-thumb'>
                      <img src='img/bg-img/p6.png' alt='' />
                    </div>
                    <div className='portfolio-meta'>
                      {/* <p className='portfolio-date'>Feb 02, 2018</p> */}
                      <h2>Master Planning</h2>
                    </div>
                  </div>
                </div>
              </div>
              {/* <div className='row justify-content-end'>
                <div className='col-12 col-md-4'>
                  <div className='single-portfolio-item portfolio-item-7 wow fadeIn'>
                    <div className='backend-content'>
                      <img
                        className='dots'
                        src='img/core-img/dots.png'
                        alt=''
                      />
                      <h2>Future</h2>
                    </div>
                    <div className='portfolio-thumb'>
                      <img src='img/bg-img/p7.png' alt='' />
                    </div>
                    <div className='portfolio-meta'>
                      <p className='portfolio-date'>Feb 02, 2018</p>
                      <h2>Mirror lake</h2>
                    </div>
                  </div>
                </div>
              </div> */}
            </div>
          </div>
          <div className='sonar-call-to-action-area section-padding-0-100'>
            <div className='backEnd-content'>
              <h2>Future</h2>
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div className='container'>
              <div className='row'>
                <div className='col-12'>
                  <div
                    className='call-to-action-content wow fadeInUp'
                    data-wow-delay='0.5s'
                  >
                    {/* <h2>We are an experienced team</h2> */}
                    <h5>Let’s talk</h5>
                    <a href='index.html#' className='btn sonar-btn mt-100'>
                      contact us
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <Footer />
        </div>
      </div>
    );
  }
}

export default Home;
