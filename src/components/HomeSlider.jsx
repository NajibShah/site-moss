import React from 'react';
import OwlCarousel from 'react-owl-carousel2';

export default function HomeSlider() {
  const options = {
    items: 1,
    nav: false,
    loop: true,
    rewind: true,
    autoplay: true,
  };

  // const events = {
  //     onDragged: function(event) {...},
  //     onChanged: function(event) {...}
  // };
  return (
    <OwlCarousel
      className='hero-slides owl-carousel'
      options={options}
      // events={events}

      margin={10}
    >
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide1.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>B-17 Islamabad</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide2.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Aquarium Mall</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide3.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Pir Sohawa Resort</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide4.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Army Public School</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide1.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Muzammilabad</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide2.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Bank Of Khyber, BoK</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide3.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Gloria Jeans</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide4.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>MOSS Interior Design</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide1.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Town Master Plan</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide2.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Najeebkot</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      {/* <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide3.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>The Desert</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className='single-hero-slide bg-img slide-background-overlay'
        style={{ backgroundImage: 'url("img/bg-img/slide4.jpg")' }}
      >
        <div className='container h-100'>
          <div className='row h-100 align-items-end'>
            <div className='col-12'>
              <div className='hero-slides-content'>
                <div className='line' />
                <h2>Mountains Hike</h2>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                  Praesent vel tortor facilisis, volutpat nulla placerat,
                  tincidunt mi. Nullam vel orci dui. Suspendisse sit amet
                  laoreet neque. Fusce sagittis suscipit sem.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div> */}
    </OwlCarousel>
  );
}
