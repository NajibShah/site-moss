import React from 'react';

export default function Loader() {
  return (
    <div>
      <div id='preloader'>
        <div className='preload-content'>
          <div id='sonar-load' />
        </div>
      </div>
    </div>
  );
}
