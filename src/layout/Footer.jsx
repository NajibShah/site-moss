import React, { Component } from 'react';

class Footer extends Component {
  render() {
    return (
      <footer className='footer-area'>
        <div className='container'>
          <div className='row'>
            <div className='col-12'>
              <div className='copywrite-text'>
                <p>
                  Copyright © All rights reserved | This website is made with{' '}
                  <i className='fa fa-heart-o' aria-hidden='true' /> by{' '}
                  <a href='https://madaviary.com' target='_blank'>
                    MadAviary
                  </a>
                </p>
              </div>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}

export default Footer;
