import React, { Component, Fragment } from 'react';

class Sidebar extends Component {
  render() {
    return (
      <Fragment>
        <div classname='grids d-flex justify-content-between'>
          <div classname='grid1'>
            <div classname='grid2'>
              <div classname='grid3'>
                <div classname='grid4'>
                  <div classname='grid5'>
                    <div classname='grid6'>
                      <div classname='grid7'>
                        <div classname='grid8'>
                          <div classname='grid9'></div>
                          <div classname='mainMenu d-flex align-items-center justify-content-between'>
                            <div classname='closeIcon'>
                              <i classname='ti-close' aria-hidden='true'></i>
                            </div>
                            <i classname='ti-close' aria-hidden='true'>
                              <div classname='logo-area'>
                                <a href='index.html'>Sonar</a>
                              </div>
                              <div
                                classname='sonarNav wow fadeInUp'
                                data-wow-delay='1s'
                              >
                                <nav>
                                  <ul>
                                    <li classname='nav-item active'>
                                      <a classname='nav-link' href='index.html'>
                                        Home
                                      </a>
                                    </li>
                                    <li classname='nav-item'>
                                      <a
                                        classname='nav-link'
                                        href='about-me.html'
                                      >
                                        About Me
                                      </a>
                                    </li>
                                    <li classname='nav-item'>
                                      <a
                                        classname='nav-link'
                                        href='services.html'
                                      >
                                        Services
                                      </a>
                                    </li>
                                    <li classname='nav-item'>
                                      <a
                                        classname='nav-link'
                                        href='portfolio.html'
                                      >
                                        Portfolio
                                      </a>
                                    </li>
                                    <li classname='nav-item'>
                                      <a classname='nav-link' href='blog.html'>
                                        Blog
                                      </a>
                                    </li>
                                    <li classname='nav-item'>
                                      <a
                                        classname='nav-link'
                                        href='contact.html'
                                      >
                                        Contact
                                      </a>
                                    </li>
                                    <li classname='nav-item'>
                                      <a
                                        classname='nav-link'
                                        href='elements.html'
                                      >
                                        Elements
                                      </a>
                                    </li>
                                  </ul>
                                </nav>
                              </div>
                              <div classname='copywrite-text'>
                                <p>
                                  Copyright © All rights reserved | This
                                  template is made with{'{'}' '{'}'}
                                  <i
                                    classname='fa fa-heart-o'
                                    aria-hidden='true'
                                  >
                                    {' '}
                                    by{'{'}' '{'}'}
                                    <a
                                      href='https://colorlib.com'
                                      target='_blank'
                                    >
                                      Colorlib
                                    </a>
                                  </i>
                                </p>
                                <i
                                  classname='fa fa-heart-o'
                                  aria-hidden='true'
                                ></i>
                              </div>
                              <i
                                classname='fa fa-heart-o'
                                aria-hidden='true'
                              ></i>
                            </i>
                          </div>
                          <i classname='ti-close' aria-hidden='true'>
                            <i classname='fa fa-heart-o' aria-hidden='true'></i>
                          </i>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </Fragment>
    );
  }
}

export default Sidebar;
